'use strict';

const fs              = require('fs');
const path            = require('path');
const parser          = require('woff2-parser');
const sass            = require('gulp-sass');

module.exports = params => {
  const { gulp, source, target, dirs, plumber, notify } = params;
  const input = `${source}/${dirs.css[0]}/font-face.scss`;
  const output = `${target}/${dirs.css[1]}`;
  let amount = 0;

  const checkNested = (obj, ...args) => {
    let clone = { ...obj };
    for (const argument of args) {
      if (!Object.prototype.hasOwnProperty.call(clone, argument)) return false;
      clone = { ...clone[argument] };
    }
    return true;
  };

  const decodeWeight = weight => {
    let number = 400;
    switch (weight) {
      case 'thin':
        number = 100;
        break;
      case 'extralight':
        number = 200;
        break;
      case 'light':
        number = 300;
        break;
      case 'regular':
        number = 400;
        break;
      case 'medium':
        number = 500;
        break;
      case 'semibold':
        number = 600;
        break;
      case 'bold':
        number = 700;
        break;
      case 'extrabold':
        number = 800;
        break;
      case 'black':
        number = 900;
        break;
      default:
        break;
    }
    return number;
  };

  const parseData = done => {
    fs.readdir(`${target}/${dirs.fonts[1]}`, (err, files) => {
      const promises = [];
      let fonts = [];
      if (files) {
        amount = files.length;
        files.forEach(file => {
          if (path.extname(file) === '.woff2') {
            promises.push(
              new Promise((resolve, reject) => {
                const i = promises.length;
                fs.readFile(`${target}/${dirs.fonts[1]}/${file}`, (err, data) => {
                  if (!err) {
                    parser(data).then(result => {
                      // fs.writeFileSync(`${target}/${dirs.fonts[1]}/${path.basename(file, '.woff2')}.json`, JSON.stringify(result, null, 2));
                      fonts[i] = searchData(file, result);
                      resolve();
                    });
                  } else reject();
                });
              })
            );
          }
        });
        Promise.all(promises).then(() => {
          fonts.sort((a, b) => {
            const an = a.name.toLowerCase();
            const bn = b.name.toLowerCase();
            const ai = a.italic;
            const bi = b.italic;
            const aw = a.weight;
            const bw = b.weight;
            if (an === bn) {
              if (ai === bi) {
                if (aw === bw) return 0;
                return aw > bw ? 1 : -1;
              }
              return ai === 'italic' ? 1 : -1;
            }
            return an > bn ? 1 : -1;
          });
          fonts = fonts.map(font => {
            const name = font.name.indexOf(' ') !== -1 ? `'${font.name}'` : font.name;
            return `(${name}, '${font.path}', ${font.weight}, ${font.italic})`;
          });
          const content = `$fonts: (\r\n\t${fonts.join(',\r\n\t')},\r\n);`;
          fs.writeFile(`${source}/${dirs.css[0]}/variables.scss`, content, () => done());
        });
      } else done();
    });
  };

  const searchData = (file, data) => {
    const font = {
      name: '',
      path: path.basename(file, '.woff2'),
      weight: 'null',
      italic: 'null'
    };
    if (checkNested(data, 'name', 'nameRecords', 'English', 'typographicFamily')) font.name = data.name.nameRecords.English.typographicFamily;
    else if (checkNested(data, 'name', 'nameRecords', 'English', 'fontFamily')) font.name = data.name.nameRecords.English.fontFamily;
    if (checkNested(data, 'OS/2', 'weight', 'value')) font.weight = data['OS/2'].weight.value;
    else if (checkNested(data, 'name', 'nameRecords', 'English', 'fontFamily')) {
      const search = /(thin|extralight|light|regular|medium|semibold|bold|extrabold|black)/.exec(data.name.nameRecords.English.fontFamily.toLowerCase());
      if (search && search[0]) font.weight = decodeWeight(search[0]);
    }
    if (checkNested(data, 'OS/2', 'selection', 'italic')) {
      if (data['OS/2'].selection.italic === true) font.italic = 'italic';
    } else if (checkNested(data, 'name', 'nameRecords', 'English', 'fontSubFamily') && data.name.nameRecords.English.fontSubFamily.toLowerCase() === 'italic') font.italic = 'italic';
    return font;
  };

  const writeData = done => amount
    ? gulp.src(input)
      .pipe(plumber({
        errorHandler: notify.onError({
          sound: false,
          title: 'css',
          message: error => error.message
        })
      }))
      .pipe(sass({ outputStyle: 'expanded' }))
      .pipe(gulp.dest(output))
    : done();

  const cleanData = async done => {
    const promises = [];
    promises.push(new Promise(resolve => {
      const url = `${output}/font-face.css`;
      fs.readFile(url, 'utf-8', (error, data) => {
        const writeData = data.replace(/"/g, "'");
        fs.writeFile(url, writeData, () => resolve());
      });
    }), new Promise(resolve => {
      fs.writeFile(`${source}/${dirs.css[0]}/variables.scss`, '$fonts: ();', () => resolve());
    }));
    await Promise.all(promises);
    done();
  };

  gulp.task('css', gulp.series(parseData, writeData, cleanData));
};