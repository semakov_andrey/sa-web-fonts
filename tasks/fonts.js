'use strict';

const fs                = require('fs');
const path              = require('path');
const ttf2woff          = require('ttf2woff');
const wawoff2           = require('wawoff2');
const replaceExtension  = require('replace-ext');
const fonteditor        = require('fonteditor-ttf');
const through2          = require('through2');
const pako              = require('pako');

module.exports = params => {
  const { gulp, source, target, dirs } = params;
  const input = `${source}/${dirs.fonts[0]}/*.{otf,ttf,woff,woff2}`;
  const output = `${target}/${dirs.fonts[1]}`;

  const toArrayBuffer = buffer => {
    const length = buffer.length;
    const view = new DataView(new ArrayBuffer(length), 0, length);
    for (let i = 0; i < length; i++) view.setUint8(i, buffer[i], false);
    return view.buffer;
  };

  const toBuffer = arrayBuffer => {
    const length = arrayBuffer.byteLength;
    const view = new DataView(arrayBuffer, 0, length);
    const buffer = new Buffer.alloc(length);
    for (let i = 0; i < length; i++) buffer[i] = view.getUint8(i, false);
    return buffer;
  };

  const convert = () => through2.obj(function (file, enc, callback) {
    const mainConvert = () => {
      if (path.extname(file.path) === '.ttf') {
        const file2 = file.clone();
        file.path = replaceExtension(file.path, '.woff');
        file.contents = new Buffer.from(ttf2woff(new Uint8Array(file.contents)).buffer);
        this.push(file);
        file2.path = replaceExtension(file2.path, '.woff2');
        wawoff2.compress(new Uint8Array(file2.contents)).then(out => {
          file2.contents = new Buffer.from(out);
          this.push(file2);
          callback();
        });
      } else {
        this.push(file);
        callback();
      }
    };

    if (file.isNull()) {
      this.push(file);
      callback();
      return;
    }
    if (path.extname(file.path) === '.otf') {
      file.path = replaceExtension(file.path, '.ttf');
      const fontObject = new fonteditor.OTFReader().read(toArrayBuffer(file.contents));
      const ttfBuffer = new fonteditor.TTFWriter().write(fonteditor.otf2ttfobject(fontObject));
      file.contents = toBuffer(ttfBuffer);
      this.push(file);
    }
    if (path.extname(file.path) === '.woff' && !fs.existsSync(`${file.path}2`)) {
      file.path = replaceExtension(file.path, '.ttf');
      file.contents = toBuffer(fonteditor.woff2ttf(toArrayBuffer(file.contents), { inflate: pako.inflate }));
      this.push(file);
    }
    if (path.extname(file.path) === '.woff2' && !fs.existsSync(file.path.slice(0, -1))) {
      file.path = replaceExtension(file.path, '.ttf');
      wawoff2.decompress(new Uint8Array(file.contents)).then(out => {
        file.contents = new Buffer.from(out);
        this.push(file);
        mainConvert();
      });
    } else mainConvert();
  });

  gulp.task('fonts', () => gulp.src(input)
    .pipe(convert())
    .pipe(gulp.dest(output)));
};