# Front-End web fonts converter. #
###### Конвертор шрифтов в форматы woff и woff2. ######

### Установка ###
- `npm init --yes`
- `npm install sa-web-fonts --save-dev`

### Основные команды ###
- `npm run convert` - конвертация
- `npm update sa-web-fonts` - обновление конвертора   
- `npm install sa-web-fonts@version` - обновление конвертора  

### Возможности ###
- **Конвертация:**
- otf -> ttf -> woff + woff2
- ttf -> woff + woff2
- single woff -> ttf -> woff + woff2
- single woff2 -> ttf -> woff + woff2
- **Копирование:**
- woff + woff2 -> woff + woff2
- **Стили font-face:**
- создание css файла по конечным шрифтам woff2

### Список тасков ###
- `fonts` - конвертация шрифтов
- `css` - создание css правил

### Используемые технологии ###
- [nodejs](https://nodejs.org/)
- [gulp](https://gulpjs.com/)
- [sass](https://sass-lang.com/) + [node-sass](https://github.com/sass/node-sass) + [gulp-sass](https://github.com/dlmanning/gulp-sass)
- [fonteditor](https://github.com/kekee000/fonteditor-ttf)
- [ttf2woff](https://github.com/fontello/ttf2woff)
- [wawoff2](https://github.com/fontello/wawoff2)