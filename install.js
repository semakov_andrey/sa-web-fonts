'use strict';

const project                   = '../../';
const fs                        = require('fs');
const path                      = require('path');
const ncp                       = require('ncp');

if (path.basename(path.resolve(__dirname, '../')) !== 'node_modules') process.exit();

const templateJSON              = require('./package.json');
const packageJSON               = require(`${project}package.json`);

/* update readme */
ncp.ncp(path.resolve(__dirname, 'readme.md'), path.resolve(__dirname, project, 'readme.md'), error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: readme updated'));

/* update task-runner */
ncp.ncp(path.resolve(__dirname, 'gulpfile.js'), path.resolve(__dirname, project, 'gulpfile.js'), error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: task-runner updated'));

/* update tasks */
ncp.ncp(path.resolve(__dirname, 'tasks'), path.resolve(__dirname, project, 'tasks'), error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: tasks updated'));

/* update configuration */
delete templateJSON.dependencies.ncp;
const json = {
  ...packageJSON,
  scripts: {
    convert: 'gulp convert',
    template: 'node node_modules/sa-web-fonts/install.js'
  },
  devDependencies: {
    ...packageJSON.devDependencies,
    ...templateJSON.dependencies
  },
  config: {
    ...packageJSON.config,
    directories: {
      ...templateJSON.config.directories,
      ...(packageJSON.config && packageJSON.config.directories ? packageJSON.config.directories : {}),
      tasks: {
        ...templateJSON.config.directories.tasks,
        ...(packageJSON.config && packageJSON.config.directories && packageJSON.config.directories.tasks ? packageJSON.config.directories.tasks : {})
      }
    },
    tasks: [...new Set([...(packageJSON.config && packageJSON.config.tasks ? packageJSON.config.tasks : []), ...templateJSON.config.tasks])]
  }
}
fs.writeFile(path.resolve(__dirname, project, 'package.json'), JSON.stringify(json, null, 2), 'utf8', error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: configuration updated'));

/* update source */
ncp.ncp(path.resolve(__dirname, 'src'), path.resolve(__dirname, project, 'src'), error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: source updated'));

/* update gitignore */
const gitignore = ['node_modules', 'build'];
fs.writeFile(path.resolve(__dirname, project, '.gitignore'), gitignore.join('\r\n'), 'utf8', error => error ? console.error('\x1b[31m%s\x1b[0m', `Error: ${error}`) : console.log('Success: gitignore updated'));