'use strict';

const packageJSON     = require('./package.json');
const gulp            = require('gulp');
const glob            = require('glob');
const plumber         = require('gulp-plumber');
const notify          = require('gulp-notify');

const source          = packageJSON.config.directories.source;
const target          = packageJSON.config.directories.production;
const dirs            = packageJSON.config.directories.tasks;
const work            = packageJSON.config.tasks;

glob.sync('./tasks/**/*.js').map(file => require(file)({
  packageJSON,
  gulp,
  source,
  target,
  dirs,
  plumber,
  notify
}));

gulp.task('convert', gulp.series('clean', gulp.series(...work)));

gulp.task('default', gulp.series('fonts'));